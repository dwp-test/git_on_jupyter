# Instructions to configure Git to handle Jupyter notebooks

1. Take the following script and put it into your repository under
`etc/filters/ipynb_output_filter.py`:


        #!/usr/bin/env python
        # -*- coding: utf-8 -*-

        """
        Suppress output and prompt numbers in git version control.  The notebooks
         themselves are not changed.

        Taken from: http://pascalbugnion.net/blog/ipython-notebooks-and-git.html.
        """
        import sys
        import json

        nb = sys.stdin.read()
        json_in = json.loads(nb)
        nb_metadata = json_in["metadata"]

        def strip_output_from_cell(cell):
            if "outputs" in cell:
                cell["outputs"] = []

            if "execution_count" in cell:
                del cell["execution_count"]

        for cell in json_in["cells"]:
            strip_output_from_cell(cell)

        json.dump(json_in,
                  sys.stdout,
                  sort_keys=True,
                  indent=1,
                  separators=(",",": "),
                  encoding="utf-8")


2. Make the file executable `$ chmod +x ipynb_output_filter.py`
3. Create `.gitattributes` and add it to your repo's root, with this inside:

        *.ipynb filter=clean_ipnb

4. Also create a file called `.gitconfig` with the code below inside and put it
   on the root of your repository.

        [filter "clean_ipynb"]
          clean = etc/filters/ipynb_drop_output.py
          smudge = cat

5. Run when inside your working directory:

        $ git config --add include.path .gitconfig 

